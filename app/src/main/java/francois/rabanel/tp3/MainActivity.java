package francois.rabanel.tp3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import francois.rabanel.tp3.adapters.UEItemAdapter;
import francois.rabanel.tp3.models.UEItem;

public class MainActivity extends AppCompatActivity {
    private List<String> ueList;
    private ListView ueListView;
    private UEItemAdapter UEadapter;
    private SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        prefs = this.getSharedPreferences("UE_LIST", Context.MODE_PRIVATE);

        // Récupérer la liste ueList
        ueList = getDataFromSharedPreferences();

        // On récupère la ListView
        ueListView = findViewById(R.id.list_view_ue);
        // L'adapter ici permet d'associer les éléments à notre listView
        UEadapter = new UEItemAdapter(this, ueList);
        ueListView.setAdapter(UEadapter);

        // Faire en sorte que le bouton et le champs texte permette de créer un nouvel
        // Objet UEItem et de l'ajouter à notre liste.
        EditText get_ue_name = (EditText) findViewById(R.id.editTextTextPersonName);
        Button add_button = (Button) findViewById(R.id.add_button);

        add_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Ici on récupère le text dans notre champs text,et on crée un nouvel objet UEItem
                // afin de l'ajouter à notre liste puis de notifier l'adapter qu'un nouvel
                // élément vient d'arriver !
                String ue_name = get_ue_name.getText().toString();
                ueList.add(ue_name);
                UEadapter.notifyDataSetChanged();
                saveDataInSharedPreferences();
            }
        });

        ListView ue_item = (ListView) findViewById(R.id.list_view_ue);
        ue_item.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent itemActivity = new Intent(MainActivity.this, ItemActivity.class);
                itemActivity.putExtra("ue_name", UEadapter.getItem(i));
                itemActivity.putExtra("index", i);

                System.out.println(UEadapter.getItem(i));
                System.out.println(UEadapter.getItem(i));
                startActivity(itemActivity);
            }
        });

        // Check action values
        int deleteAction = (int) getIntent().getIntExtra("action_delete", 0);
        int deleteIndex = (int) getIntent().getIntExtra("index_delete", 0);

        if (deleteAction == 1) {
            System.out.println(deleteAction);
            System.out.println(deleteIndex);

            String itemToRemove = UEadapter.getItem(deleteIndex);
            ueList.remove(itemToRemove);
            UEadapter.notifyDataSetChanged();
            saveDataInSharedPreferences();
        }

    }

    protected void saveDataInSharedPreferences(){
        StringBuilder storedUeList = new StringBuilder();

        // On construit notre liste comme une seule et meme string
        for (int i = 0; i < ueList.size(); i++) {
            storedUeList.append(ueList.get(i)).append(",");
        }
        // Puis on écrit notre string à l'endroit que l'on souhaite lié à la clé "ueList"
        prefs.edit().putString("ueList", storedUeList.toString()).apply();
    }

    protected List<String> getDataFromSharedPreferences(){
        String name = prefs.getString("ueList", "");

        if (name.isEmpty()){
            // Si on a aucune donnée enregistrées, on créé une nouvelle liste avec mes UE de ce
            // semestre
            ueList = new ArrayList<>();
            ueList.add("SMB116");
            ueList.add("SMB111");
            ueList.add("NFP107");
            ueList.add("RCP105");

            return ueList;
        } else {
            // Si on a déjà des données, on récupère notre string "item1,item2,item3"
            // qu'on va reformater correctement pour récréer notre ArrayList.
            String[] items = name.split(",");
            ueList = new ArrayList<>();
            for(int i=0; i < items.length; i++){
                ueList.add(items[i]);
            }

            return ueList;
        }

    }

}


