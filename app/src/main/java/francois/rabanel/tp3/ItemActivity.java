package francois.rabanel.tp3;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class ItemActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item);

        String ueName = (String) getIntent().getSerializableExtra("ue_name");
        int ueIndex = (int) getIntent().getIntExtra("index", 0);

        TextView ueNameTextView = (TextView) findViewById(R.id.ue_name_text_view);
        ueNameTextView.setText(ueName);
        Toast.makeText(
                ItemActivity.this,
                ueName,
                Toast.LENGTH_SHORT
        ).show();

        // Build Alert Dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Confirm");
        builder.setMessage("Are you sure?");

        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Do nothing but close the dialog
                Intent mainActivity = new Intent(ItemActivity.this, MainActivity.class);
                mainActivity.putExtra("action_delete", 1);
                mainActivity.putExtra("index_delete", ueIndex);

                dialog.dismiss();
                startActivity(mainActivity);
            }
        });
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Quit the Activity
                dialog.dismiss();
                finish();
            }
        });

        AlertDialog alert = builder.create();
        Button deleteButton = (Button)findViewById(R.id.delete_button);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert.show();
            }
        });
    }
}