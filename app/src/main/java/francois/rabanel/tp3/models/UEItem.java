package francois.rabanel.tp3.models;

public class UEItem {
    private String name;

    public UEItem(String name) {
        this.name = name;
    }

    public String getName() { return name; }
}
