package francois.rabanel.tp3.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import francois.rabanel.tp3.R;
import francois.rabanel.tp3.models.UEItem;

public class UEItemAdapter extends BaseAdapter {
    private Context context;
    private List<String> ueItemList;
    private LayoutInflater inflater;
    private int counter;

    public UEItemAdapter(Context context, List<String> ueItemList){
        this.context = context;
        this.ueItemList = ueItemList;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        // on récupère ici la liste d'item en fonction du nombre qu'on aura dans la list
        return ueItemList.size();
    }

    @Override
    public String getItem(int i) {
        // récupère tel élément de notre liste.
        Log.d("hello", "click");
        return ueItemList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    // our ViewHolder.
    // caches our TextView
    static class ViewHolderItem {
        TextView textViewItem;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolderItem viewHolder;

        if (view==null) {
            // Permet d'injecter le nombre d'élément dans notre liste dans notre ListView
            // Quand c'est la premiere fois qu'on charge notre vue, sachant que view==null
            view = inflater.inflate(R.layout.adapter_item, null);

            viewHolder = new ViewHolderItem();
            viewHolder.textViewItem = (TextView) view.findViewById(R.id.ue_text_view);

            // On stock le contenu de notre view dans viewHolder
            view.setTag(viewHolder);
        } else {
            this.counter += 1;
            viewHolder = (ViewHolderItem) view.getTag();
        }

        // On récupère l'élément et on ajoute le nom de l'UE
        String ueName = getItem(i);
        if (!ueName.isEmpty()){
            viewHolder.textViewItem.setText(ueName);
        }

        Log.d("COUNTER", "value: " + counter);

        return view;
    }
}
